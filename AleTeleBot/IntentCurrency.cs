using HtmlAgilityPack;
using Telegram.Bot.Types;

namespace AleTeleBot;

public class IntentCurrency: IIntent
{
    private static string[] termCurrency =
        { "currency", "валют", "$" };
    public bool haveIntent(Message message)
    {
        if (string.IsNullOrEmpty(message.Text)) return false;
        return termCurrency.Any(term => message.Text.ToLower().Contains(term));
    }
    public async Task<string> doReaction(Message message)
    {
        using var client = new HttpClient();
        client.DefaultRequestHeaders.Add("User-Agent", "C# console program");

        var content = await client.GetStringAsync("https://www.cbr.ru/currency_base/daily/");

        HtmlDocument htmlDocument = new HtmlDocument();
        htmlDocument.LoadHtml(content);

        List<List<string>> currences = htmlDocument.DocumentNode.SelectSingleNode("//table[@class='data']")
            .Descendants("tr")
            // .Skip(1)
            .Where(tr => tr.Elements("td").Count() > 1)
            .Select(tr => tr.Elements("td").Select(td => td.InnerText.Trim()).ToList())
            .ToList();

        List<string> targetCurrency = new List<string>() { "USD", "EUR", "CNY" };

        string result = String.Empty;
        
        foreach (var currency in currences)
        {
            if (currency.Count == 5)
            {
                if (targetCurrency.Contains(currency.ElementAt(1)))
                {
                    string msg = $"{currency.ElementAt(2)} {currency.ElementAt(1)} = {currency.ElementAt(4)} RUR";
                    // Console.WriteLine($"{msg}");
                    result += $"\n{msg}";
                }
            }
        }
        return result;
    }
}