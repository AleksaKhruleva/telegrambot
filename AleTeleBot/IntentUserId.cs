using Telegram.Bot.Types;

namespace AleTeleBot;

public class IntentUserId: IIntent
{
    private static string[] termUserId =
        { "userid", "юзерид", "мой ид", "uid", "юид", "уид" };
    public bool haveIntent(Message message)
    {
        if (string.IsNullOrEmpty(message.Text)) return false;
        return termUserId.Any(term => message.Text.ToLower().Contains(term));
    }

    public async Task<string> doReaction(Message message)
    {
        return $"UserId: {message.From.Id}/@{message.From.Username}.";
    }
}