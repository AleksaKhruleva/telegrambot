using System.Data;
using System.Text.RegularExpressions;
using Telegram.Bot.Types;

namespace AleTeleBot;

public class IntentCalculator: IIntent
{
    public bool haveIntent(Message message)
    {
        if (string.IsNullOrEmpty(message.Text)) return false;
        Regex regex = new Regex(@"[\d\-\+\*\/\(\)]{3,}");
        MatchCollection matches = regex.Matches(message.Text);
        return matches.Count > 0;
    }

    public async Task<string> doReaction(Message message)
    {
        Regex regex = new Regex(@"[\d\-\+\*\/\(\)]{3,}");
        MatchCollection matches = regex.Matches(message.Text);
        if (matches.Count < 1) return "An error when evaluate expression...";
        DataTable dataTable = new DataTable();
        string result = String.Empty;
        foreach (Match match in matches)
        {
            try
            {
                result += $"\n{match.Value} = {dataTable.Compute(match.Value, "").ToString()}";
            }
            catch (Exception)
            {
                return "An error when evaluate expression...";
            }
        }
        return result;
    }
}