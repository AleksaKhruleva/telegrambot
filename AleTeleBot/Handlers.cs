using AleTeleBot;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;


public static class Handlers
{
    private static List<IIntent> _intents = new List<IIntent>() { new IntentHello()
                                                                , new IntentUserId()
                                                                , new IntentCalculator()
                                                                , new IntentCurrency() };
    public static async Task UpdateHandler(
        ITelegramBotClient client,
        Update update,
        CancellationToken ct)
    {
        // Console.WriteLine(JsonSerializer.Serialize(update));
        if (update.Type is UpdateType.Message && update.Message is not null)
        {
            var message = update.Message;
            if (message.From.IsBot)
            {
                SendText(client, message, ct, @"Съешь ещё этих мягких французских булок, да выпей же чаю...");
            }
            else
            {
                bool isIntent = false;
                foreach (var intent in _intents.Where(intent => intent.haveIntent(message)))
                {
                    isIntent = true;
                    SendText(client, message, ct, intent.doReaction(message).Result);
                }
                if (!isIntent)
                {
                    SendText(client, message, ct, @"Съешь ещё этих мягких французских булок, да выпей же чаю...");
                }
            }
        }
    }

    public static async Task ErrorHandler(
        ITelegramBotClient client,
        Exception ex,
        CancellationToken ct)
    {
    }

    private static async void SendText(ITelegramBotClient client, Message message, CancellationToken ct, string text)
    {
        await client.SendTextMessageAsync(
            message.Chat.Id,
            text,
            cancellationToken: ct);
    }
}