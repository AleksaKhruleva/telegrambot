using Telegram.Bot.Types;

namespace AleTeleBot;

public class IntentHello: IIntent
{
    private static string[] intentHello =
        { "hi", "здравствуй", "добрый день", "доброе утро", "добрый вечер", "привет", "салют", "хэлло", "хай" };
    private static int termHelloI = 0;
    private static string[] reactionHello =
        { "Здравствуй", "Привет", "Салют", "Хэлло", "Хай" };
    public bool haveIntent(Message message)
    {
        /*MorphAnalyzer morphAnalyzer = new MorphAnalyzer();
        var morphInfos = morphAnalyzer.Parse(message.Text).ToArray();
        foreach (var morphInfo in morphInfos)
        {
            Console.WriteLine(morphInfo.ToString());
        }*/
        if (string.IsNullOrEmpty(message.Text)) return false;
        return intentHello.Any(term => message.Text.ToLower().Contains(term));
    }

    public async Task<string> doReaction(Message message)
    {
        return $"{reactionHello[(termHelloI++) % reactionHello.Length]}, {message.From.FirstName}!";
    }
}