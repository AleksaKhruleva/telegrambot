using Telegram.Bot.Types;

namespace AleTeleBot;

public interface IIntent
{
    public bool haveIntent(Message message);
    public Task<string> doReaction(Message message);
}